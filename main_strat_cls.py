from strategy_cls.acquisition_compiler import AcquisitionCompiler
from strategy_cls.compiler import Compiler
from strategy_cls.idle_compiler import IdleCompiler
from strategy_cls.pulse_compiler import PulseCompiler


def main():
    schedule = [
        IdleCompiler(100),
        PulseCompiler(4),
        AcquisitionCompiler(0, 1, 200),
    ]
    Compiler(schedule).run()


if __name__ == "__main__":
    main()
