from time import sleep


class InstrumentCoordinator:
    def __init__(self) -> None:
        self._acquisition: list[int] = []

    def prepare(self) -> None:
        print("Preparing instrument coordinator")

    def start(self) -> None:
        print("Starting experiment")

    def stop(self) -> None:
        print("Stopping experiment")
        self._acquisition = [1, 5, 12, 12, 2, 45]

    def retrieve_acquisition(self) -> list[int]:
        return self._acquisition

    def wait_done(self) -> None:
        print("Blocking until done...")
        sleep(2)
