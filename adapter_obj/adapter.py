from time import sleep

from adapter_obj.qblox import InstrumentCoordinator
from adapter_obj.zhinst import ZhinstModule


class ZhinstInstrumentCoordinator(InstrumentCoordinator):
    def __init__(self, zhinst_module: ZhinstModule) -> None:
        super().__init__()
        self.zhinst_module = zhinst_module

    def prepare(self) -> None:
        self.zhinst_module.clear()

    def start(self) -> None:
        self.zhinst_module.execute()

    def stop(self) -> None:
        self.zhinst_module.finish()

    def retrieve_acquisition(self) -> list[int]:
        return self.zhinst_module.get()

    def wait_done(self) -> None:
        while not self.zhinst_module.finished():
            sleep(1)
