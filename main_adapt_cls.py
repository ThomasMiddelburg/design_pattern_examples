from adapter_cls.adapter import ZhinstInstrumentCoordinator
from adapter_cls.qblox import InstrumentCoordinator


def main():
    inst_coord = InstrumentCoordinator()
    # inst_coord = ZhinstInstrumentCoordinator()
    inst_coord.prepare()
    inst_coord.start()
    inst_coord.wait_done()
    inst_coord.stop()
    print(inst_coord.retrieve_acquisition())


if __name__ == "__main__":
    main()
