class ZhinstModule:
    def __init__(self) -> None:
        super().__init__()
        self._data: list[int] = []
        self._finished_checked = 0

    def clear(self) -> None:
        print("Clearing module...")

    def execute(self) -> None:
        print("Executing...")

    def finish(self) -> None:
        print("Finished")
        self._data = [3, 36, 8, 51, 4, 25, 4, 19, 8]

    def get(self) -> list[int]:
        return self._data

    def finished(self) -> bool:
        print("Checking if finished...")
        self._finished_checked += 1
        return self._finished_checked == 3
