from time import sleep

from adapter_cls.qblox import InstrumentCoordinator
from adapter_cls.zhinst import ZhinstModule


class ZhinstInstrumentCoordinator(ZhinstModule, InstrumentCoordinator):
    def prepare(self) -> None:
        self.clear()

    def start(self) -> None:
        self.execute()

    def stop(self) -> None:
        self.finish()

    def retrieve_acquisition(self) -> list[int]:
        return self.get()

    def wait_done(self) -> None:
        while not self.finished():
            sleep(1)
