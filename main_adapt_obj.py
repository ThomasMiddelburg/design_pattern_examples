from adapter_obj.adapter import ZhinstInstrumentCoordinator
from adapter_obj.qblox import InstrumentCoordinator
from adapter_obj.zhinst import ZhinstModule


def main():
    # inst_coord = InstrumentCoordinator()
    inst_coord = ZhinstInstrumentCoordinator(ZhinstModule())

    inst_coord.prepare()
    inst_coord.start()
    inst_coord.wait_done()
    inst_coord.stop()
    print(inst_coord.retrieve_acquisition())


if __name__ == "__main__":
    main()
