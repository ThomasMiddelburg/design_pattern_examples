from strategy_fun.backend_compiler import compile_backend
from strategy_fun.qblox_compiler import qblox_compiler
from strategy_fun.zhinst_compiler import zhinst_compiler


def main():
    # compilation_fn = qblox_compiler
    compilation_fn = zhinst_compiler

    instructions = ["play", "reset_phase", "acquire", "wait"]
    compile_backend(instructions, compilation_fn)


if __name__ == "__main__":
    main()
