from typing import Callable


def compile_backend(
    instructions: list[str], compilation_fn: Callable[[list[str]], list[str]]
):
    print("Compiling...")
    print(compilation_fn(instructions))
    print("Done")
