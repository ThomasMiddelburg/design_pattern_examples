def qblox_compiler(instructions: list[str]) -> list[str]:
    return ["qblox-compiled " + instr for instr in instructions]
