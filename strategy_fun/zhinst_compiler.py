def zhinst_compiler(instructions: list[str]) -> list[str]:
    return ["zhinst-compiled " + instr for instr in instructions]
