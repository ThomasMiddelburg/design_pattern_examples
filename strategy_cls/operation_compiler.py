from abc import ABC, abstractmethod


class OperationCompiler(ABC):
    @abstractmethod
    def generate_qasm(self) -> list[str]:
        pass
