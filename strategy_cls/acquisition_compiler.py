from strategy_cls.operation_compiler import OperationCompiler


class AcquisitionCompiler(OperationCompiler):
    def __init__(self, acq_channel: int, bin_idx: int | str, duration_ns: int) -> None:
        self.acq_channel = acq_channel
        self.bin_idx = bin_idx
        self.duration_ns = duration_ns

    def generate_qasm(self) -> list[str]:
        return ["acquire", f"{self.acq_channel},{self.bin_idx},{self.duration_ns}"]
