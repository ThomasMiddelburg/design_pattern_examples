from strategy_cls.operation_compiler import OperationCompiler


class PulseCompiler(OperationCompiler):
    def __init__(self, duration_ns: int) -> None:
        self.duration_ns = duration_ns

    def generate_qasm(self) -> list[str]:
        return ["play", f"0,1,{self.duration_ns}"]
