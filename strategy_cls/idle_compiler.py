from strategy_cls.operation_compiler import OperationCompiler


class IdleCompiler(OperationCompiler):
    def __init__(self, duration_ns: int) -> None:
        self.duration_ns = duration_ns

    def generate_qasm(self) -> list[str]:
        return ["wait", f"{self.duration_ns}"]
