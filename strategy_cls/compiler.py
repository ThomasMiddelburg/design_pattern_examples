from columnar import columnar

from strategy_cls.operation_compiler import OperationCompiler


def pretty_print(cols: list[list[str]]) -> None:
    print(columnar(cols, row_sep="", column_sep=" "))


class Compiler:
    def __init__(self, schedule: list[OperationCompiler]) -> None:
        self.schedule = schedule

    def run(self) -> None:
        self.determine_timing()
        self.assign_registers()
        pretty_print([op.generate_qasm() for op in self.schedule])

    def determine_timing(self) -> None:
        print("Determining absolute timing")

    def assign_registers(self) -> None:
        print("Assigning variables to registers")
